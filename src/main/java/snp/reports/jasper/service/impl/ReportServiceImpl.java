package snp.reports.jasper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import snp.reports.jasper.dao.ReportDao;
import snp.reports.jasper.domain.Report;
import snp.reports.jasper.service.ReportService;

import java.util.List;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportDao reportDao;
    @Override
    public List<Report> getReport() {
        return reportDao.getReport();
    }
}
