package snp.reports.jasper.service;

import snp.reports.jasper.domain.Report;

import java.util.List;

public interface ReportService {
    List<Report> getReport();
}
