package snp.reports.jasper.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    @Bean
    @Qualifier("defaultDataSource")
    @Primary
    @ConfigurationProperties(prefix = "default.datasource")
    DataSource defaultDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @Qualifier("defaultJdbcTemplate")
    JdbcTemplate defaultJdbcTemplate(@Qualifier("defaultDataSource") DataSource defaultDataSource) {
        return new JdbcTemplate(defaultDataSource);
    }
}

