package snp.reports.jasper.dao;

import snp.reports.jasper.domain.Report;

import java.util.List;

public interface ReportDao {
    List<Report> getReport();
}
