package snp.reports.jasper.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import snp.reports.jasper.dao.ReportDao;
import snp.reports.jasper.domain.Report;

import java.util.Collections;
import java.util.List;

@Transactional
@Repository
public class ReportDaoImpl implements ReportDao {

    @Autowired
    @Qualifier("defaultJdbcTemplate")
    JdbcTemplate defaultJdbcTemplate;

    @Override
    public List<Report> getReport() {
        try {
            String sql = "SELECT * FROM report";
            return defaultJdbcTemplate.query(
                    sql,
                    new Object[]{},
                    new BeanPropertyRowMapper<>(Report.class)
            );
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }
}
