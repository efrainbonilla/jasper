package snp.reports.jasper.web.rest;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import snp.reports.jasper.domain.Report;
import snp.reports.jasper.service.ReportService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ReportRestController {

    @Autowired
    private ReportService reportService;

    private void report(HttpServletResponse response, String jasper) throws JRException, IOException {
        InputStream jasperStream1 = this.getClass().getResourceAsStream("/reports/" + jasper);

        List<Report> list = reportService.getReport();
        Map<String, Object> parameters = new HashMap<>();

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream1);

        JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(list, false);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, source);

        response.setHeader("Content-disposition", "inline; filename="+ Math.random() +".pdf");
        response.setContentType("application/x-pdf");
        final ServletOutputStream outStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
    }

    @GetMapping("utf8")
    public void utf8Pdf(HttpServletResponse response) throws JRException, IOException {
        report(response, "utf8.jasper");
    }
}
