--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: report; Type: TABLE; Schema: public; Owner: senapi
--

CREATE TABLE public.report (
    id bigint NOT NULL,
    title text NOT NULL
);


ALTER TABLE public.report OWNER TO senapi;

--
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: senapi
--

CREATE SEQUENCE public.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_id_seq OWNER TO senapi;

--
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: senapi
--

ALTER SEQUENCE public.report_id_seq OWNED BY public.report.id;


--
-- Name: report id; Type: DEFAULT; Schema: public; Owner: senapi
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.report_id_seq'::regclass);


--
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: senapi
--

COPY public.report (id, title) FROM stdin;
1	´`^¨~=>|°µ _-,;:!¡?¿/.·'‘’"“”()[]{}®@$£*&#%+–•…♡♥빠오️\t\n\r012²3³456789aáàâäãªbcçdðeéèêëfghiíìîïjklĺmnñoóòôöõøōºpqrsšßtuúùûüvwxyýÿzαҫ天遍鸿
2	´`^¨~<=>|¦° _-,;:!¡?¿/.·'‘’"“”»()[]{}®@$*\\&#%+±–—„•…♡♥\t\n\r012²3³456789aáàâäãªbcçdeéèêëfghiíìïjklmnñoóòôöõºpqrstuúùüvwxyÿz
3	´`¨<=>|° _-,;:!¡?/.·'‘’"“”()[]}™$*&#%+́̃–\t\n\r0123456789aáàâäãªæbcçdðeéèêëfghiíìîjklłmnñoóòôöõøºpqrsšşßtuúùüvwxyýzż 
4	´`¨=>° _-,;:!¡?¿/.·'’"“”()[]*&%+̃–•…\t\n\r012²3456789aáàäãbcçdðeéèêëfghiíìïjklmnñoóòôöõºpqrstuúüvwxyz 
5	´`=| _-,;:!/.'’"“”()&%+–\t\n\r0123456789aáàäãªbcçdðeéèêfghiíjklmnñoóòôöõºpqrstuúüvwxyz
6	´`=>° _-,:!¡?¿/.'’"“”()@*&#%+–•�\t\n\r0123456789aáàäãªbcçdðeéèêëfghiíïjklmnñoóòôöºpqrstuúüvwxyz
7	´`¨=° _-,;:!?/.'’"“”()[]*&#%+–\t\n\r0123456789aáàäãªbcçdeéèêëfghiíìjklmnñoóòôöõøºpqrsšßtuúüvwxyz 
8	´` _-,!¡/.'’"“”()}@*&#%+–\t\n\r0123456789aáâbcdðeéêfghiíjklmnñoóòºpqrstuúüvwxyz
9	´`¨° -,:!¡?¿/.'’"“”()[]{}@*&%+–…\n\r0123456789aáâäãbcçdðeéèêfghiíîjklmnñoóôöºpqrstuúüvwxyz
10	´`= -,;:!¡?/.·'‘’"“”()*&%+́–\t\n\r0123456789aáàäãªbcçdðeéèêfghiíìjklmnñoóòôöõpqrstuúüvwxyz
11	YÍĞÍT AKÜ MALZEMELERÍ NAKLÍYAT TURÍZM ÍNŞAAT SANAYÍ VE TÍCARET ANONÍM ŞÍRKETÍ.
12	ELŻBIETA INGLOT ZBIGNIEW INGLOT SPÓŁKA CYWILNA\tUl. Grunwaldzka 62A, PL-37-700 Przemyśl, Poland
\.


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: senapi
--

SELECT pg_catalog.setval('public.report_id_seq', 12, true);


--
-- Name: report report_pk; Type: CONSTRAINT; Schema: public; Owner: senapi
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_pk PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

